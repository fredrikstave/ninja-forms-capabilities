<?php

/**
 * Remove Ninja Forms menu page for all users except administrator
 *
 * @author Fredrik Stave
 * @since 1.0.0
 */
if (!function_exists('nfc_unregister_menu_pages')) {
	function nfc_unregister_menu_pages() {
		global $current_user;
		$user_caps = $current_user->caps;
		$is_admin = current_user_can('manage_options');
		$menu_page_cap = apply_filters('nfc_standalone_submissions_cap', 'edit_pages');

		if (in_array($menu_page_cap, $user_caps) && !$is_admin) {
			remove_submenu_page('ninja-forms', 'edit.php?post_type=nf_sub');
			remove_menu_page('ninja-forms');
		}
	}
}
add_action('admin_menu', 'nfc_unregister_menu_pages', 999);

/**
 * Add standalone Ninja Forms submissions menu page for user
 * that fulfill the nfc_standalone_submissions_cap
 *
 * @author Fredrik Stave
 * @since 1.0.0
 */
if (!function_exists('nfc_register_menu_pages')) {
	function nfc_register_menu_pages() {
		global $current_user;
		$user_caps = $current_user->caps;
		$is_admin = current_user_can('manage_options');

		$menu_page_cap = apply_filters('nfc_standalone_submissions_cap', 'edit_pages');

		if (in_array($menu_page_cap, $user_caps) && !$is_admin) {
			$label = apply_filters(
				'nfc_standalone_submissions_menu_label',
				__('Submissions', 'ninja-forms')
			);
			$icon = apply_filters(
				'nfc_standalone_submissions_menu_icon',
				'dashicons-feedback'
			);
			add_menu_page(
				$label,
				$label,
				apply_filters('nfc_standalone_submissions_cap', 'edit_pages'),
				'edit.php?post_type=nf_sub',
				null,
				$icon,
				'25.7'
			);
		}
	}
}
add_action('admin_menu', 'nfc_register_menu_pages', 999);