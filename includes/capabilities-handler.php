<?php
/**
 * Add capability management
 *
 * @author Fredrik Stave
 * @version 1.0.0
 */

/**
 * Give editors access to the Ninja Forms submissions menu
 *
 * @author Fredrik Stave
 * @since 1.0.0
 */
function nfc_all_forms_capabilities_filter($capabilities) {
    $capabilities = apply_filters('nfc_standalone_submissions_cap', 'edit_pages');
    return $capabilities;
}
add_filter('ninja_forms_admin_parent_menu_capabilities', 'nfc_all_forms_capabilities_filter');

/**
 * Give editors access to the Ninja Forms submissions
 *
 * @author Fredrik Stave
 * @since 1.0.0
 */
function nfc_subs_cap_filter($cap) {
	$cap = apply_filters('nfc_standalone_submissions_cap', 'edit_pages');
    return $cap;
}
add_filter('ninja_forms_admin_submissions_capabilities', 'nfc_subs_cap_filter');