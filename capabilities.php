<?php
/*
Plugin Name: Ninja Forms - Capabilities
Plugin URI: http://www.07.no
Description: Capabilities add-on for Ninja Forms.
Version: 1.0.0
Author: Fredrik Stave
Author URI: http://www.07interaktiv.no

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

*/

define("NF_CAPABILITIES_DIR", WP_PLUGIN_DIR."/".basename( dirname( __FILE__ ) ) );
define("NF_CAPABILITIES_VERSION", "1.0.0");

require_once(NF_CAPABILITIES_DIR . '/includes/capabilities-handler.php');
require_once(NF_CAPABILITIES_DIR . '/includes/register-menu-pages.php');