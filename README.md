# Ninja Forms Capabilities Add-On

## About the plugin
This plugin is a Ninja Forms add-on that adds a standalone *Submissions* menu page and hides the *Forms* page.
Such a plugin is very desireable in those cases where you have lots of custom modifications in your clients forms
which, if edited, would break the form layout or functionality.

Since Ninja Forms' built-in filters don't support such functionality, or allow fine grained user cap administration,
this plugin has been developed in order to solve the issue in a graceful manner.

## Install
To install the plugin, there are two equally simple ways of doing it.

### Clone the GIT repository
You can clone this git repository into your ```/wp-content/plugins``` directory.

```
1. cd /path/to/project/wp-content/plugins
2. git clone git@gitlab.com:fredrikstave/ninja-forms-capabilities.git
3. Activate the plugin from the WP admin panel under Installed plugins
```

### Download repository as a ZIP archive
1. Click the ![alt text](http://i.imgur.com/rxB1qg2.png) icon on the right hand side of the git project address
2. Install the plugin in WordPress by uploading it in WP admin and activate it

Then you're done. Enjoy!

## Filters
There are two filters built into the plugin to give developers control over basic functionality.
These filters can be leveraged in the projects functions.php file.

### nfc_standalone_submissions_cap
This filter defines at what capability level the plugin will take effect. The default kicks in when user are registered
as editors (or has been granted the ```edit_pages``` capability).

Example of use:

```
// Grant authors access to submissions
function your_override_function($cap) {
	return 'edit_posts';
}
add_filter('nfc_standalone_submissions_cap', 'your_override_function');
```

### nfc_standalone_submissions_menu_label
This filter allows you to change the text for the menu item on the left hand side in the WP admin area.

Example of use:
```
// Add Norwegian text in the menu item
function your_override_function($label) {
	return 'Innsendinger';
}
add_filter('nfc_standalone_submissions_menu_label', 'your_override_function');
```

### nfc_standalone_submissions_menu_icon
This filter will allow you to choose another icon for the *Submissions* menu item. Default icon is set 
to ```dashicons-feedback```, which is also the icon used by Ninja Forms.

Example of use:
```
function your_override_function($icon) {
	return 'dashicons-exerpt-view';
}
add_filter('nfc_standalone_submissions_menu_icon', 'your_override_function');
```

*Note that this filter only works with dashicons at the moment. Version 1.1.0 may include more sophisticated filters
for adding custom icons.*

## Further development towards version 2.0.0
This plugin works pretty well *as is*, but for the future, it would be nice to implement som WP admin settings capabilities to eliminate the need for filters (though these will be kept and refined in v2.0.0). It would also be nice to add translation capabilities to the project. This could also be outsourced in a separate *Localization* add-on for Ninja Forms.